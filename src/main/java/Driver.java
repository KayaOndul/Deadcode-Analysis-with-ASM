import org.apache.commons.lang3.StringUtils;
import sun.awt.image.ImageWatched;

import javax.security.auth.login.CredentialException;
import javax.xml.soap.Node;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Driver {
    private static final String PATH = "/home/kaya/Downloads/proj/src/main/java/TestData.java";
    private static final String READPATH = "target/classes/TestData.class";


    public static void main(String[] args) throws IOException {


        ControlFlowGraph controlFlowGraph = new ControlFlowGraph();
        UseDefAnalyzer useDefAnalyzer = new UseDefAnalyzer();

        List<String> dotList = controlFlowGraph.createCFG(PATH, READPATH);

        List<Map> firstDefThenUseListMaps = useDefAnalyzer.getUseDefPairs(PATH, READPATH);
        HashMap<Integer, Set<Integer>> nodeDefs = (HashMap<Integer, Set<Integer>>) firstDefThenUseListMaps.get(0);
        HashMap<Integer, Set<Integer>> nodeUses = (HashMap<Integer, Set<Integer>>) firstDefThenUseListMaps.get(1);


        useDefAnalyzer.putUseDefsToFiles(dotList, nodeDefs, nodeUses);


        for (String readPath : dotList) {
            File f = new File(readPath);
            FileInputStream stream = new FileInputStream(f);
            InputStreamReader conexion = new InputStreamReader(stream);
            LineNumberReader reader = new LineNumberReader(conexion);
            HashMap<Integer, String> labels = new HashMap<>();
            String x;
            HashMap<Integer, Set<Integer>> use = new HashMap<>();
            HashMap<Integer, Set<Integer>> def = new HashMap<>();

            TreeSet<NodeSet> nodes = new TreeSet<>();
            while ((x = reader.readLine()) != null) {
                labels.put(reader.getLineNumber(), x);

            }

            NodeSet finalN = null;

            for (Map.Entry entry : labels.entrySet()) {
                String value = (String) entry.getValue();
                if (value.contains("[")) {
                    int indexAngularBracket = value.indexOf('[');
                    Integer nodeNum = Integer.parseInt(value.substring(0, indexAngularBracket).trim());
                    String[] uses = StringUtils.substringBetween(value, "USE: [", "]").split(",");
                    String[] defs = StringUtils.substringBetween(value, "DEF: [", "]").split(",");
                    putVariables(use, nodeNum, uses);
                    putVariables(def, nodeNum, defs);


                }

                if (!value.contains("[")
                        && !value.contains("START")
                        && !value.contains("FINISH")
                        && !value.contains("digraph")
                        && !value.contains("}")
                        && !value.equals("")

                ) {
                    String[] arr = value.split(" -> ");
                    Integer from = Integer.parseInt(arr[0].trim());

                    Integer to = Integer.parseInt(arr[1].trim());

                    NodeSet n1 = containsLine(nodes, from) ? new ArrayList<NodeSet>(nodes).get(0) : null;
                    if (n1 == null) {
                        n1 = new NodeSet(from);
                        n1.def = nodeDefs.get(from) == null ? new TreeSet<>() : nodeDefs.get(from);
                        n1.use = nodeUses.get(from) == null ? new TreeSet<>() : nodeUses.get(from);
                        n1.in = new TreeSet<>();
                    }
                    n1.addSuccessor(to);

                    NodeSet n2 = containsLine(nodes, to) ? new ArrayList<NodeSet>(nodes).get(0) : null;
                    if (n2 == null) {
                        n2 = new NodeSet(from);
                        n2.def = nodeDefs.get(from) == null ? new TreeSet<>() : nodeDefs.get(from);
                        n2.use = nodeUses.get(from) == null ? new TreeSet<>() : nodeUses.get(from);
                        n1.in = new TreeSet<>();
                    }

                    n2.addPredecessor(from);


                    nodes.add(n1);
                    nodes.add(n2);


                }
                if (value.contains("FINISH")) {
                    String[] arr = value.split(" -> ");
                    Integer last = Integer.parseInt(arr[0].trim());

                    NodeSet n1 = containsLine(nodes, last) ? new ArrayList<NodeSet>(nodes).get(0) : null;
                    if (n1 == null) {
                        n1 = new NodeSet(last);
                        n1.def = nodeDefs.get(last) == null ? new TreeSet<>() : nodeDefs.get(last);
                        n1.use = nodeUses.get(last) == null ? new TreeSet<>() : nodeUses.get(last);

                    }

                    finalN = n1;

                    for (NodeSet e : nodes) {
                        if (e.successors.contains(last)) {
                            finalN.addPredecessor(e.lineNumber);
                        }
                    }

                    finalN.out = new TreeSet<>();
                    finalN.in = new TreeSet<>();
                    nodes.add(finalN);


                }


            }


            LinkedList<NodeSet> changedNodes = new LinkedList<>();
            TreeSet<NodeSet> finalResult = new TreeSet<>();
            changedNodes.addAll(nodes);

            while (!changedNodes.isEmpty()) {

                NodeSet n = changedNodes.removeFirst(); //Remove i from Changed Nodes

                Set<Integer> oldIn = n.in;
                Set<Integer> oldOut = n.out;


                Set<Integer> newIn = new TreeSet<>();
                newIn.addAll(n.use);
                Set<Integer> copyOldOut = new TreeSet<>(oldOut);
                copyOldOut.removeAll(n.def);
                newIn.addAll(copyOldOut);
                n.in = newIn;


                Set<Integer> successorsIn = new TreeSet<>();
                for (Integer e : n.successors) {
                    List<NodeSet> nodeSetList = nodes.stream().filter(a -> a.lineNumber.equals(e)).collect(Collectors.toList());
                    if(nodeSetList.size()>0){
                        successorsIn.addAll(nodeSetList.get(0).in);
                    }

                }

                n.out = successorsIn;


                if (!oldIn.equals(newIn) || !oldOut.equals(successorsIn)) {
                    changedNodes.add(n);
                } else {

                    finalResult.add(n);
                }
            }
            Set<Integer> deadOnes = new TreeSet<>();

            finalResult.forEach(e -> {
                Set<NodeSet> succ = nodes.stream().filter(el -> e.successors.contains(el.lineNumber)).collect(Collectors.toSet());
                for (Integer integer : e.def) {
                    Set<Integer> ins = new HashSet<>();
                    for (NodeSet nodeSet : succ) {
                        ins.addAll(nodeSet.in);
                    }
                    if (!ins.contains(integer)) {
                        deadOnes.add(e.lineNumber);
                    }

                }
            });


            deadOnes.forEach(e -> {
                labels.entrySet().forEach(a -> {
                    String val = a.getValue();
                    if (val.contains("label") && e.equals(Integer.parseInt(val.split(" ")[0].trim()))) {

                        val = val.substring(0, val.length() - 2) + "\n DEAD" + '"' + "]";


                    }

                    labels.replace(a.getKey(), val);
                });
            });


            File fout = new File(readPath);
            FileOutputStream fos = new FileOutputStream(fout);

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            labels.entrySet().forEach(line -> {
                try {
                    bw.write(line.getValue()+"\n");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
            bw.close();


        }


    }

    private static void printUtils(String fileName, TreeSet<NodeSet> finalResult) {
        System.out.println(fileName.toUpperCase());
        System.out.printf("%10s%10s%10s%10s%10s\n", "PROGRAM POINT ", "DEF", "USE", "IN", "OUT");
        finalResult.forEach(e -> {
            System.out.printf("%3s%21s%10s%10s%10s\n", e.lineNumber, e.def, e.use, e.in, e.out);

            System.out.println("-----------------------------------");


        });
    }

    private static boolean containsLine(TreeSet<NodeSet> nodeSets, Integer integer) {
        for (NodeSet nodeSet : nodeSets) {
            if (nodeSet.lineNumber.equals(integer)) {
                return true;
            }
        }
        return false;
    }

    private static void putHierarchy(HashMap<Integer, Set<Integer>> pred, Integer from, Integer to) {
        Set<Integer> _temp;
        if (!pred.containsKey(to)) {
            _temp = new HashSet<>();
        } else {
            _temp = pred.get(to);
        }
        _temp.add(from);
        pred.put(to, _temp);
    }

    private static void putVariables(HashMap<Integer, Set<Integer>> map, Integer nodeNum, String[] defs) {
        Set<Integer> _temp;
        if (!map.containsKey(nodeNum)) {
            _temp = new HashSet<>();
        } else {
            _temp = map.get(nodeNum);
        }
        Set<Integer> final_temp1 = _temp;
        Arrays.asList(defs).forEach(e ->
                {
                    if (e.equals("")) {
                        return;
                    }
                    final_temp1.add(Integer.parseInt(e.trim()));
                }
        );
        map.put(nodeNum, final_temp1);
    }


}
