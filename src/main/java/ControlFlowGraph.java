import jdk.internal.org.objectweb.asm.ClassReader;
import jdk.internal.org.objectweb.asm.Opcodes;
import jdk.internal.org.objectweb.asm.tree.*;

import java.io.*;
import java.util.*;

public class ControlFlowGraph {


    public List<String> createCFG(String path, String readPath) throws IOException {
        InputStream in = new FileInputStream(readPath);


        File f = new File(path);
        FileInputStream stream = new FileInputStream(f);
        InputStreamReader conexion = new InputStreamReader(stream);
        LineNumberReader reader = new LineNumberReader(conexion);
        HashMap<Integer, String> labels = new HashMap<>();
        String x;


        while ((x = reader.readLine()) != null) {
            labels.put(reader.getLineNumber(), x);

        }


        ClassReader cr = new ClassReader(in);
        ClassNode classNode = new ClassNode();

        //ClassNode is a ClassVisitor
        cr.accept(classNode, ClassReader.EXPAND_FRAMES);
        List<MethodNode> methods = classNode.methods;


        List<String> list = new ArrayList<String>();
        for (MethodNode methodNode : methods) {
            if (methodNode.name.equals("<init>"))
                continue;

            InsnList instructions = methodNode.instructions;


            AbstractInsnNode abstractInsnNode;


            List<String> printList = new ArrayList<>();


            createPrintList(instructions, printList);
            BufferedWriter bw = printDotsToFile(labels, list, printList);
            bw.close();

        }


        return list;
    }

    private void createPrintList(InsnList instructions, List<String> printList) {
        AbstractInsnNode abstractInsnNode;
        for (int i = 0; i < instructions.size(); ++i) {
            abstractInsnNode = instructions.get(i);
            if (!(abstractInsnNode == null && abstractInsnNode.getNext() == null)) {
                jumpRelations(abstractInsnNode, printList);
                insnRelations(abstractInsnNode, printList);
                frameRelations(abstractInsnNode, printList);
            }
            if (abstractInsnNode.getPrevious() == null) {
                firstRelations(abstractInsnNode, printList);
            }
            if(abstractInsnNode.getNext()!=null&&abstractInsnNode.getNext().getOpcode()== Opcodes.RETURN){
                returnRelations(abstractInsnNode,printList);
            }
            if (abstractInsnNode.getNext() == null) {
                lastRelations(abstractInsnNode, printList);
            }
        }
    }

    private BufferedWriter printDotsToFile(HashMap<Integer, String> labels, List<String> list, List<String> printList) throws IOException {
        StringBuilder sb = new StringBuilder();
        String nameMethod = labels.get(Integer.parseInt(printList.get(1).split(" -> ")[0]) - 1).trim().split(" ")[2];
        int loc = nameMethod.indexOf("(");
        nameMethod = nameMethod.substring(0, loc);
        list.add(String.format("Outputs/%s.txt", nameMethod));
        String fileName = String.format("Outputs/%s.txt", nameMethod);
        File fout = new File(fileName);
        FileOutputStream fos = new FileOutputStream(fout);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        bw.write("digraph " + nameMethod + "{" + "\n");
        printList.stream().distinct().forEach(e -> {

            try {

                bw.write(e);
                if (Character.isDigit(e.trim().substring(0, 1).toCharArray()[0])) {
                    String inter = e.trim().substring(0, 3).replace("-", " ").trim();
                    Integer lineNum = Integer.parseInt(inter);
                    String search = labels.get(lineNum).trim();
                    sb.append(lineNum).append(" [label=").append('"').append(" L# ").append(lineNum).append(" ").append(search.trim().replace('"', '`')).append('"').append("]").append("\n");
                }

                bw.newLine();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        bw.newLine();
        bw.write(sb.toString());
        bw.write("}");

        return bw;
    }
    private List returnRelations(AbstractInsnNode abstractInsnNode, List print) {
        AbstractInsnNode backward = abstractInsnNode.getPrevious();
        AbstractInsnNode forward = abstractInsnNode;

        while (backward != null && backward.getType() != AbstractInsnNode.LINE) {
            if (backward.getType() == AbstractInsnNode.JUMP_INSN)
                return print;
            backward = backward.getPrevious();

        }
        while (forward != null && forward.getType() != AbstractInsnNode.LINE) {
            if (forward.getType() == AbstractInsnNode.JUMP_INSN)
                return print;
            forward = forward.getNext();

        }
        addToPrintList(print, backward, forward);
        return print;
    }
    private List lastRelations(AbstractInsnNode abstractInsnNode, List print) {
        if (abstractInsnNode.getType() == AbstractInsnNode.LABEL && abstractInsnNode.getOpcode() == -1) {
            AbstractInsnNode backward = abstractInsnNode;
            while (backward != null && backward.getType() != AbstractInsnNode.LINE) {
                backward = backward.getPrevious();
            }

            String s;
            s = (((LineNumberNode) backward).line + " -> " + "FINISH");
            print.add(s.trim());
        }
        return print;
    }

    private List firstRelations(AbstractInsnNode abstractInsnNode, List print) {
        AbstractInsnNode forward = abstractInsnNode;
        while (forward != null && forward.getType() != AbstractInsnNode.LINE) {
            forward = forward.getNext();
        }

        String s;
        s =  "START"+" -> " +(((LineNumberNode) forward).line );
        print.add(s.trim());
        return print;
    }

    private List frameRelations(AbstractInsnNode abstractInsnNode, List print) {
        if (abstractInsnNode.getType() == AbstractInsnNode.FRAME) {
            AbstractInsnNode backward = abstractInsnNode;
            AbstractInsnNode forward = abstractInsnNode;

            while (backward != null && backward.getType() != AbstractInsnNode.LINE) {
                if (backward.getType() == AbstractInsnNode.JUMP_INSN)
                    return print;
                backward = backward.getPrevious();

            }
            while (forward != null && forward.getType() != AbstractInsnNode.LINE) {
                if (forward.getType() == AbstractInsnNode.JUMP_INSN)
                    return print;
                forward = forward.getNext();

            }
            addToPrintList(print, backward, forward);
        }

        return print;

    }

    private void addToPrintList(List print, AbstractInsnNode backward, AbstractInsnNode forward) {
        if ((backward != null && forward != null) && forward.getType() == AbstractInsnNode.LINE) {
            String s;
            s = (((LineNumberNode) backward).line + " -> ");
            s += (((LineNumberNode) forward).line);
            print.add(s.trim());
        }
    }


    private List jumpRelations(AbstractInsnNode abstractInsnNode, List print) {

        if (abstractInsnNode.getType() == AbstractInsnNode.JUMP_INSN) {
            AbstractInsnNode tmp = abstractInsnNode;
            AbstractInsnNode fwd = abstractInsnNode;
            while (tmp != null && tmp.getType() != AbstractInsnNode.LINE) {
                tmp = tmp.getPrevious();

            }
            while (fwd != null && fwd.getType() != AbstractInsnNode.LINE) {
                fwd = fwd.getNext();

            }


            if (tmp.getType() == AbstractInsnNode.LINE && (((JumpInsnNode) abstractInsnNode).label.getNext()).getType() != AbstractInsnNode.FRAME) {
                String s;
                s = (((LineNumberNode) tmp).line + " -> ");
                s += (((LineNumberNode) (((JumpInsnNode) abstractInsnNode).label.getNext())).line);
                print.add(s.trim());
            }


        }

        return print;
    }

    private List insnRelations(AbstractInsnNode abstractInsnNode, List print) {

        if (abstractInsnNode.getType() == AbstractInsnNode.LABEL) {

            AbstractInsnNode backward = abstractInsnNode;
            AbstractInsnNode forward = abstractInsnNode;
            while (forward != null && forward.getType() != AbstractInsnNode.LINE) {
                forward = forward.getNext();

            }


            while (backward != null && backward.getType() != AbstractInsnNode.LINE) {
                if (backward.getType() == AbstractInsnNode.METHOD_INSN && forward.getNext().getType() != AbstractInsnNode.IINC_INSN)
                    return print;
                backward = backward.getPrevious();

            }

            if (forward != null && forward.getNext().getType() == AbstractInsnNode.FRAME && forward.getPrevious().getPrevious().getType() == AbstractInsnNode.JUMP_INSN) {
                return print;
            }
            addToPrintList(print, backward, forward);
        }

        return print;

    }


}

