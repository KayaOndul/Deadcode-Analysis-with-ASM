digraph test1{
START -> 7
7 -> 8
8 -> 10
10 -> 17
10 -> 11
11 -> 14
11 -> 12
12 -> 23
14 -> 23
17 -> 20
17 -> 18
18 -> 23
20 -> 23
23 -> FINISH

7 [label=" L# 7 int x = 3 USE: [] DEF: [1] "]
8 [label=" L# 8 int y = 5 USE: [] DEF: [2] "]
10 [label=" L# 10 if (x <= 2)  USE: [1] DEF: [] "]
11 [label=" L# 11 if (y >= 4)  USE: [2] DEF: [] "]
12 [label=" L# 12 System.out.println(`x <=2 and y >= 4`) USE: [] DEF: [] "]
14 [label=" L# 14 System.out.println(`x <= 2 and y < 4`) USE: [] DEF: [] "]
17 [label=" L# 17 if (y >= 4)  USE: [2] DEF: [] "]
18 [label=" L# 18 System.out.println(`x > 2 and y >= 4`) USE: [] DEF: [] "]
20 [label=" L# 20 System.out.println(`x < 2 and y < 4`) USE: [] DEF: [] "]
23 [label=" L# 23  USE: [] DEF: [] "]
}
