import java.util.Set;
import java.util.TreeSet;


public class NodeSet implements Comparable<NodeSet> {
    public NodeSet(Integer lineNumber) {
        this.lineNumber = lineNumber;
        this.predecessors = new TreeSet<>();
        this.successors = new TreeSet<>();
        this.use = new TreeSet<>();
        this.def = new TreeSet<>();
        this.in = new TreeSet<>();
        this.out = new TreeSet<>();
    }

    Integer lineNumber;
    Set<Integer> predecessors;
    Set<Integer> successors;
    Set<Integer> use;
    Set<Integer> def;
    Set<Integer> in;
    Set<Integer> out;


    public void addPredecessor(Integer integer) {
        predecessors.add(integer);
    }

    public void addSuccessor(Integer integer) {
        successors.add(integer);
    }




    @Override
    public int compareTo(NodeSet nodeSet) {
        return nodeSet.lineNumber.compareTo(this.lineNumber);



    }
}
