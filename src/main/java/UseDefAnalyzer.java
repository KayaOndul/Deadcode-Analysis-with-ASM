import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import jdk.internal.org.objectweb.asm.ClassReader;
import jdk.internal.org.objectweb.asm.Opcodes;
import jdk.internal.org.objectweb.asm.tree.*;

import java.io.*;
import java.util.*;

import static java.util.Map.Entry.comparingByKey;
import static java.util.stream.Collectors.toMap;

public class UseDefAnalyzer {
    public List<Map> getUseDefPairs(String path, String readPath) throws IOException {

        InputStream in = new FileInputStream(readPath);


        File f = new File(path);
        FileInputStream stream = new FileInputStream(f);
        InputStreamReader conexion = new InputStreamReader(stream);
        LineNumberReader reader = new LineNumberReader(conexion);
        HashMap<Integer, String> labels = new HashMap<>();
        String x;


        while ((x = reader.readLine()) != null) {
            if (x.length() < 6)
                continue;
            labels.put(reader.getLineNumber(), x);

        }


        ClassReader cr = new ClassReader(in);
        ClassNode classNode = new ClassNode();

        //ClassNode is a ClassVisitor
        cr.accept(classNode, ClassReader.EXPAND_FRAMES);
        List<MethodNode> methods = classNode.methods;


        HashMap<Integer, Set<Integer>> nodeUses = new HashMap<>();
        HashMap<Integer, Set<Integer>> nodeDefs = new HashMap<>();
        for (MethodNode methodNode : methods) {
            if (methodNode.name.equals("<init>"))
                continue;

            InsnList instructions = methodNode.instructions;


            List<String> printList = new ArrayList<>();


            AbstractInsnNode abstractInsnNode;
            for (int i = 0; i < instructions.size(); ++i) {
                abstractInsnNode = instructions.get(i);
                if (!(abstractInsnNode == null && abstractInsnNode.getNext() == null)) {
                    if (abstractInsnNode.getOpcode() == Opcodes.IINC) {
                        logDefinition(nodeDefs, abstractInsnNode);
                        logUse(nodeUses, abstractInsnNode);
                    } else if (abstractInsnNode.getOpcode() == Opcodes.ILOAD) {
                        logUse(nodeUses, abstractInsnNode);
                    } else if (abstractInsnNode.getOpcode() == Opcodes.ISTORE) {
                        logDefinition(nodeDefs, abstractInsnNode);
                    }


                }
            }


        }

        Map sortedNodeDefs = nodeDefs.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (e1, e2) -> e2, LinkedHashMap::new));
        Map sortedNodeUses = nodeUses.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (e1, e2) -> e2, LinkedHashMap::new));

        List<Map> retList = new ArrayList<>();
        retList.add(sortedNodeDefs);
        retList.add(sortedNodeUses);

        return retList;

    }
    public void putUseDefsToFiles( List<String> dotList,HashMap<Integer, Set<Integer>> nodeDefs,HashMap<Integer, Set<Integer>> nodeUses) throws IOException {

        for (int i = 0; i < dotList.size(); ++i) {
            String path = dotList.get(i);

            File f = new File(path);
            FileInputStream stream = new FileInputStream(f);
            InputStreamReader conexion = new InputStreamReader(stream);
            LineNumberReader reader = new LineNumberReader(conexion);
            String x;


            List<String> strings = new ArrayList<>();
            while ((x = reader.readLine()) != "\n") {
                String sb = "";
                if (x == null) {
                    break;
                }
                if (!x.contains("[")) {
                    sb += x + "\n";
                    strings.add(sb);
                    continue;
                }
                try {
                    Integer lineNum = Integer.parseInt(x.split(" ")[0]);
                    int sizeRow = x.length();
                    String useAddition;
                    String defAddition;
                    if (nodeUses.get(lineNum) != null) {
                        useAddition = "USE: " + nodeUses.get(lineNum).toString() + " ";
                    } else {
                        useAddition = "USE: [] ";
                    }

                    if (nodeDefs.get(lineNum) != null) {
                        defAddition = "DEF: " + nodeDefs.get(lineNum).toString() + " ";
                    } else {
                        defAddition = "DEF: [] ";
                    }

                    String toAdd = x.substring(0, sizeRow - 3) + " " + useAddition + defAddition + '"' + "]\n";

                    sb += (toAdd);
                } catch (Exception ex) {
                    sb += x;

                }

                strings.add(sb);
            }
            File fout = new File(path);
            FileOutputStream fos = new FileOutputStream(fout);

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            strings.stream().distinct().forEach(e-> {
                try {
                    bw.write(e);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
            bw.close();


        }
    }
    private void logDefinition(HashMap<Integer, Set<Integer>> nodeDefs, AbstractInsnNode abstractInsnNode) {
        AbstractInsnNode backward = abstractInsnNode;
        Integer varNum;
        if (abstractInsnNode.getType() == AbstractInsnNode.IINC_INSN) {
            varNum = ((IincInsnNode) abstractInsnNode).var;
        } else {
            varNum = ((VarInsnNode) abstractInsnNode).var;
        }

        while (backward != null && backward.getType() != AbstractInsnNode.LINE) {
            backward = backward.getPrevious();
        }
        if (backward == null) {
            return;
        }
        Integer lineNum = ((LineNumberNode) backward).line;

        Set<Integer> _temp;
        if (!nodeDefs.containsKey(lineNum)) {
            _temp = new HashSet<>();
        } else {
            _temp = nodeDefs.get(lineNum);
        }
        _temp.add(varNum);
        nodeDefs.put(lineNum, _temp);
    }

    private void logUse(HashMap<Integer, Set<Integer>> nodeUses, AbstractInsnNode abstractInsnNode) {
        AbstractInsnNode backward = abstractInsnNode;
        Integer varNum;
        if (abstractInsnNode.getType() == AbstractInsnNode.IINC_INSN) {
            varNum = ((IincInsnNode) abstractInsnNode).var;
        } else {
            varNum = ((VarInsnNode) abstractInsnNode).var;
        }
        while (backward != null && backward.getType() != AbstractInsnNode.LINE) {
            backward = backward.getPrevious();
        }
        if (backward == null) {
            return;
        }
        Integer lineNum = ((LineNumberNode) backward).line;

        Set<Integer> _temp;
        if (!nodeUses.containsKey(lineNum)) {
            _temp = new HashSet<>();
        } else {
            _temp = nodeUses.get(lineNum);
        }
        _temp.add(varNum);
        nodeUses.put(lineNum, _temp);
    }


    private void createPrintList(InsnList instructions, List<String> printList) {
        AbstractInsnNode abstractInsnNode;
        for (int i = 0; i < instructions.size(); ++i) {
            abstractInsnNode = instructions.get(i);
            if (!(abstractInsnNode == null && abstractInsnNode.getNext() == null)) {
                if (abstractInsnNode.getType() == AbstractInsnNode.VAR_INSN) {

                }
            }
        }
    }


}
