digraph ForTest{
START -> 110
110 -> 112
112 -> 113
113 -> 120
113 -> 116
116 -> 114
114 -> 113
120 -> FINISH

110 [label=" L# 110 int x = 3 USE: [] DEF: [1] "]
112 [label=" L# 112 for (int i = 0 USE: [] DEF: [2] "]
113 [label=" L# 113 i < x USE: [1, 2] DEF: [] "]
116 [label=" L# 116 System.out.println(`i`) USE: [] DEF: [] "]
114 [label=" L# 114 i++)  USE: [2] DEF: [2] "]
120 [label=" L# 120  USE: [] DEF: [] "]
}
